import java.util.Scanner;
class Point
{
    public
    int []p = null;
    int Size = 0;
    Point(int i)
    {
        p = new int[i];
        Size = i;
    }

    //Copy Constructor...
 
   public Point(Point x)
{
		
		System.out.println("\nThe Copy Constructor Is Called Here");
		Size = x.Size;
	
                    p = new int[Size];
    		}

    void equal(Point x)
    {
        if(Size==x.Size)
        {
            System.out.println("\nPoints have equal dimension");
        }
        else
        {
            System.out.println("\nPoints have different dimension");
        }
    }
}

class Main
{
    public static void main(String []args)
    {
        Scanner s = new Scanner(System.in);
	        	Point []pt = null;
	
		pt = new Point[5];
		int x,i,j,k;
		for(i=0;i<pt.length;i++)
        {
            System.out.println("\nEnter the dimension of the array - "+i);
            		x = s.nextInt();
            		pt[i] = new Point(x);
        }
        for(j=0;j<pt.length-1;j++)
        {
            System.out.println("\nComparison with other remaining arrays of " +j);
            for(k=j+1;k<pt.length;k++)
            {
                pt[j].equal(pt[k]);
            }
        }
        System.out.println("");
        Point p = new Point(pt[0]);
        pt[0].equal(p);
        System.out.println("\nAs the copy constructor is same as pt[0] so they are equal always");
    }
}
