#include<iostream>
#include<cstdlib>
#include <ctime>
using namespace std;

class Sales //class Declaration for Sales account program
{    //variables declaration
        string itemname[50];
        char customer_name[30];

        public: // public so that they can be accessed easily
        int customer_id,contact_detail,total_items,Quantity[4];
		float prod_price[4];
		void getData();  // member function which are made outside class
		void putData();
		void getBill();
};
// input customer name and  his/her details
void Sales::putData()//member function to  input the customer details from the customer
{
	cout<<" \n Enter Customer Name : ";
	cin>>customer_name;
	cout<<" \n Enter Customer ID: ";
	cin>>customer_id;
	cout<<"\n Enter Contact No.";
	cin>>contact_detail;
	cout<<" \n How many items the customer has purchased?";
	cin>>total_items;
	for(int i=0;i<total_items;i++)//for loop to let user enter the details of all the products he/her has bought
	{
		cout<<"\n Enter Product Name  "<<i+1<<": ";
		cin>>itemname[i];
        cout<<"\n Price"<<i+1<<": Rs. ";
		cin>>prod_price[i];
		cout<<"\n No. of Items: ";
		cin>>Quantity[i];
		prod_price[i]=Quantity[i]*prod_price[i];
	}
}
void Sales::getData()//member function for  printing purchase details
{

	cout<<"Name of item\tQuantity \tPrice\n";
	cout<<"------------------------------------------------------------------------"<<endl;
	for(int i=0;i<total_items;i++)
	{
		cout<<"Name: "<<itemname[i]<<" \t  Qty: "<<Quantity[i]<<" \t Price: "<<prod_price[i]<<endl;
    }
	cout<<"------------------------------------------------------------------------"<<endl;

	}

void Sales::getBill()// member function for printing the bill
{
	getData();
	float cash=0,totalcost=0,qty=0;
	for(int i=0;i<total_items;i++)// Calculation for printing the total cost as well as the total quantity
	{
		qty+=Quantity[i];
        totalcost=totalcost+prod_price[i];
	}
	cout<<"Total:------------------------------------------------------------------------";
	cout<<"\n\t\t Quantity= "<<qty<<"\t Rupees= "<<totalcost;
	cout<<"\n******************************************************************************";
	pay:
	cout<<"\n Billing  Details\n";
	cout<<" \n Total cash given is Rupees : ";
	cin>>cash;
	if(cash>=totalcost)
		cout<<"Change to be given back is Rupees: "<<cash-totalcost<<endl;
	else
	{	cout<<"You have paid less amount";

		goto pay;// If paid amount is less than this goto func. will get back where pay is called
	}
}
int main()
{
                        time_t now = time(0); //Billing Starting time
                        char* dt = ctime(&now);
	cout<<"*****************************************************************************\n";
	cout<<"\t \t \tWELCOME TO OUR STORE \t"<<dt<<"*";
	cout<<"****************************************************************************\n";

	Sales cust; // Declaratiion of object
	int option;
	cout<<" First PRESS 1 to Add a product in the cart and then PRESS 2 to get the BILL"<<endl;
	start:
	cout<<"\n Enter 1 to Add a New sales Record";
	cout<<"\n Enter 2 to get the Bill"<<endl;
	cout<<" Enter 3 EXIT"<<endl;
	cout<<" Enter your choice: ";
	cin>>option;
	switch(option)//For selecting the option
	{
// to print the detail and to get the bill ,switch case is used as it is to be driven it can also be done using if- else if..
		case 1:{
		    cust.putData();//calling the member function for inputing the customer detail
			cout<<"\nItems Entered Successfully... \n";
			goto start;
			break;
                }
		case 2: {
            cust.getBill();// to call the bill member fuction
            time_t now = time(0);
            char* dt = ctime(&now);
            cout <<"Billing time:" << dt << endl;
            cout<<"\n Thankyou and Have a good Day! Visit Again...\n ";
            goto start;
            break;
                }
        		case 3:{
		    cout<<"\n Thankyou and Have a good Day! Visit Again...\n ";
			exit(0);
		                }

	}

}
