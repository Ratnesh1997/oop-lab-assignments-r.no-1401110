#include<iostream>
#include<cstdlib>
#include <ctime> //Header file for getting time
using namespace std;

class Sales //class Declaration for Sales account program
{
     //variables declaration

      private:
        char customer_name[30];

        public: // public so that they can be accessed easily
        string itemname[5];
        int customer_id,contact_detail;
		void getData();
		void putData();
};

class Bill:private Sales// Use of inherirance is done as the properties of Sales are needed in class Bill
{            public:
                        int Quantity[4],total_items;// max 4 quantity can be added
                        float prod_price[4];
                        Bill(){// Constructor bill is made " Constructor name is same as class name "
		cout<<" \n How many items the customer has purchased?";
        cin>>total_items;
        for(int i=0;i<total_items;i++)//for loop to let user enter the details of all the products he/her has bought
        {
		cout<<"\n Enter Product Name  "<<i+1<<": ";
		cin>>itemname[i];
        cout<<"\n Price"<<i+1<<": Rs. ";
		cin>>prod_price[i];
		cout<<"\n No. of Items: ";
		cin>>Quantity[i];
        }
		float cash=0,sum=0,qty=0;// Initializing these variablea
      for(int i=0;i<total_items;i++){
		prod_price[i]=Quantity[i]*prod_price[i];
                qty+=Quantity[i];   // Calculation  for total Quantity
                sum=sum+prod_price[i];//Calculation of total Sum
        }
	cout<<"Name of item\tQuantity \tPrice\n";
	cout<<"------------------------------------------------------------------------"<<endl;
	for(int i=0;i<total_items;i++)
	{
		cout<<"Name: "<<itemname[i]<<" \t  Qty: "<<Quantity[i]<<" \t Price: "<<prod_price[i]<<endl;
    }
	cout<<"------------------------------------------------------------------------"<<endl;

	cout<<"Total:------------------------------------------------------------------------";
	cout<<"\n\t\t Quantity= "<<qty<<"\t Rupees= "<<sum;
	cout<<"\n******************************************************************************";
	pay:
	cout<<"\n Billing  Details\n";
	cout<<" \n Total cash given is Rupees : ";
	cin>>cash;
	if(cash>=sum)
		cout<<"Change to be given back is Rupees: "<<cash-sum<<endl;
	else
	{
	    cout<<"Kindly Pay the Required Amount you have paid less";
		goto pay;
	}
}
    ~Bill();// Using Destructor to free the memory
};
        Bill::~Bill(){
                    cout<<"Object is being deleted use of destructor"<<endl;
                     }
// input customer name and  his/her details
void Sales::putData()
{
	cout<<" \n Enter Customer Name : ";
	cin>>customer_name;
	cout<<" \n Enter Customer ID: ";
	cin>>customer_id;
	cout<<"\n Enter Contact No.";
	cin>>contact_detail;
}
// For printing the customer's Details on the screen
void Sales::getData()
{
    cout<<"\n The Customer name is: "<<customer_name<<endl;
    cout<<" The Customer ID is: "<<customer_id<<endl;
    cout<<" The Customer's contact no. is: "<<contact_detail<<endl;
}
int main()
{
            time_t now = time(0);// to get the time took the help of google
            char* dt = ctime(&now);
    cout<<"***********************************************************************\n";
	cout<<" \t  WELCOME TO OUR STORE \t"<<dt<<"*";
	cout<<"***********************************************************************\n";
    Sales b1;// b1 is the object of Sales class
	int option;
	cout<<" First PRESS 1 to Add the customer details and then PRESS 2 to get the BILL"<<endl;
	start://Using goto function
	cout<<"\n Enter 1 to Add a New Customer Record";// Creating a Menu
	cout<<"\n Enter 2 to purchase and then get the Bill"<<endl;
	cout<<" Enter 3 to know customer's details"<<endl;
	cout<<" Enter 4 to EXIT"<<endl;
	cout<<" Enter your choice: ";
	cin>>option;
	switch(option)
	{
		case 1:{
		    b1.putData();//member function putdata is called here to let user enter the details
			cout<<"Items Entered Successfully... \n";
			goto start;
			break;
		}
		case 2: {
            Bill c1;// Object of Constructor Bill is created
            time_t now = time(0);
            char* dt = ctime(&now);
            cout <<"Billing time:" << dt << endl;
            cout<<"\n Thankyou and Have a good Day! Visit Again...\n ";
            goto start;
            break;
                }
         case 3:{

                b1.getData();//member function getData is getting called here
                 goto start;
                break;
         }
		case 4:{
		    cout<<"\n Thankyou and Have a good Day! Visit Again...\n ";
			exit(0);//to exit the program
                }
	}
}

