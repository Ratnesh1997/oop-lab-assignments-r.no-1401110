/* You Have to Change System Date to Run The get INterest Function */



import java.util.*;
abstract class Account{                  //abstract parent class Account

private String accNo;
protected double balance;				//kept protected so that it can be used in it's child class

public Account(String no,double bal){	//Constructor of Account class
				accNo=no;
				balance=bal;
				}

public String get_accNo(){               // get Account No is common for savings and current therefore it has been kept in Account class
				return accNo;			 // return Accounts No
}
//to display balance and it is common for both child classes therefore it has been kept in parent class
public double get_balance()
{				return balance;			
}

public boolean deposit(double amount){	//deposit member function is common for savings and current therefore it has been kept in Account class
	
				amount=amount;
				if(amount>=0){			//If Amount is positive then Only Deposit is Successful 
						balance += amount;
						System.out.println("Deposit Successful"); 
						return true;
				}

				else{
						System.out.println("Deposit is Invalid!!");  // If Amount entered is negative than deposit is unsuccessful
						return false;
				}
			}

public abstract boolean withdraw(double amount1); //only prototype has been defined so that it can be implemented in dynamic polymorphism
	//kept abstract so that any user who would create other child classes of Account has to create this member function and declare it's method
	
public abstract boolean get_interest(int month);  // Get Interest method is Only For Savings Account but Since using object a1 we need to make 
													//in Account class as abstract
public abstract float get_InterestRate();		// returns Rate of Interest
public abstract double get_Odl();				// prototype to get OverDraft Limit
}

class Savings extends Account{                   //child class of Account

private float interestRate;
Calendar cale=Calendar.getInstance();
public Savings(String no,float Rate){			//Constructor of child class Savings
				super(no,1000);					//calling Constructor of parent class Account using 'super' keyword

				interestRate=Rate;
			}

public float get_InterestRate(){			// member Function to Show Interest Rate Already  set By the Bank
				return interestRate;
			}
public double get_Odl(){						// Body of get_Odl in Savings Class 
            System.out.println("This Facility is not available for Savings Account");
            return (000);						
}
public boolean withdraw(double amount1){       // withdraw Member Function for Savings Account
				if(amount1<=(balance-500)){		//Minimum Balance in the Account must be Rs 500 so used this 
							balance=balance - amount1;
							System.out.println("Withdraw Successful!!");
							return true;
						            }
				else
					{ System.out.println("Withdraw Unsuccessful!!");
						return false;
					}
				 }
public boolean get_interest(int month){       //to calculate interest
								double time=((((cale.get(Calendar.MONTH))+1)-month)/12f);
								double Rupees=(1+interestRate/400);	 
													if((((cale.get(Calendar.MONTH))+1)-month)>=3 && month<=12){ // Checks that difference in Month of Account created and the present Month is more than or =3  
																				balance=balance*Math.pow(Rupees,4*time);// Formula for Compound Interest is p*(1+r/n)^nt  where p=principle , r=rate of Interest,n=no. of Interests Compounded per Year,t=to calculate balance for t years  
								System.out.println("t in years"+time);   
								//System.out.println("balance:"+balance); 
								//System.out.println("Rupees are:"+Rupees);
								return true;
								}
								else{
									balance=balance;
									return false;
									}
										}
}


class Current extends Account{  	//second child class of parent class Account

private double overDraft_lmt;
public Current(String no,double ovdl){ //Constructor of child class Current
	
				super(no,0);			//calling parent class Constructor of Account
				if((ovdl>=500)&&(ovdl<=10000000))				//overdraft limit has to be between "500"rs and "1Cr"rs.
	 					overDraft_lmt=ovdl;
				else{
                    System.out.println("Invalid OverDraft Limit");
                    overDraft_lmt = 500;    					// If overdraft Limit is out of range minimum amount is 500
				}}
		public double get_Odl(){
							return overDraft_lmt; 				// returns OverDraft Limit
					}
        public float get_InterestRate(){
                    System.out.println("This Facility is not available for Current Account");
                    return (999f);
					}
		public boolean withdraw(double amount1){				//withdraw member function for Current account
	
					if(amount1<=overDraft_lmt+balance){//customer can withdraw money till the negative balance upto  overdraft limit
			
							balance=balance-amount1;
							System.out.println("Withdraw Successful"); // If above Condition is satisfied than Withdraw is Successful
							return true;
									}
					else{
							System.out.println("Withdraw UnSuccessfull");
							return false;
						}
				}
		public boolean get_interest(int month){
			System.out.println("This Facility is not available for Current Account");
			return true;
	}
			}

class Banking{			//main class
public static void displayMenu(){   //Function to Display Menu 
			System.out.println("Enter 1-->  To Check Balance");
			System.out.println("Enter 2-->  To Deposit");
			System.out.println("Enter 3-->  To Withdraw");
			System.out.println("Enter 4-->  To Calculate the Interest And Balance After Certain period");
			System.out.println("Enter 5-->  To Get the Current Interest Rate");
			System.out.println("Enter 6-->  To Get the Current OverDraft Limit");
			System.out.println("Enter 7-->  Exit");
			System.out.println("Enter Your Choice...");

}
public static void main(String args[]){//main function
Scanner input=new Scanner(System.in);
int ch,ch1;
			System.out.println("Enter 1-->  To Create Savings Account");
			System.out.println("Enter 2-->  To Create Current Account");
			ch=input.nextInt();
			Account a1;//since Account is an abstract class therefore only it's reference can be created and we can't instantiate it
	// Here Dynamic Polymorphism is used , according to previous choice made object of that class will be created at run time.
			if(ch==1){
					a1=new Savings("1000098",10f); // Default Account no is passed and rate of Interest is set to 10%
				}
			else
					a1=new Current("1000001",1000000); //Default Account no is passed and OverDraft Limit is set to 1o Lakh
			

			while(true){

				displayMenu();
				ch1=input.nextInt();
			if(ch1==1){ // to Check Balance
				System.out.println("The Current Balance :"+a1.get_balance());
				}
			else if(ch1==2){ // To Deposit 
				System.out.println("Enter Amount You want to Deposit");
				a1.deposit(input.nextDouble());
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());

				}
			else if(ch1==3){ // To WithDraw
				System.out.println("Enter Amount You Withdraw");
				a1.withdraw(input.nextDouble());
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());
				}
			else if(ch1==4){
				if(ch==1){    //To Get the Interest between the period of Month in which account is created and the present month and interest is added Quaterly
					System.out.println("Enter The Month in which you created the Account in the present Year");
					a1.get_interest(input.nextInt());
					System.out.println("The Current Balance :"+a1.get_balance());
				}
				else{
					System.out.println("This Facility Is Available Only For Savings Account");
				}

			}
			else if(ch1==5){ // To get Interest Rate of Bank
				if(ch==1){
					System.out.println("The Current Interest Rate in % is:" +a1.get_InterestRate());
				}
				else{
					System.out.println(a1.get_InterestRate());
				}
            }
            else if(ch1==6){
				if(ch==1){ // To Know OverDraft limit provided to you by the Bank
					System.out.println("The Current OverDraft Limit is:" +a1.get_Odl());
				}
				else{
					System.out.println("The OverDraft Limit is Rs:"+a1.get_Odl());
				}

			}
			else { // To Exit
				System.exit(0);
			         }
				}
}

}
