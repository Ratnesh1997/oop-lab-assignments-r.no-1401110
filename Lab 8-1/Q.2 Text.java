// Program to Count No. of Words,Characters and Lines 
import java.util.*;
import java.io.*;

class Text
{
    public static void main(String []args)
    {
        String filename;
        int choice = 0;
        Scanner input1 = new Scanner(System.in);
        System.out.println("Enter the file name");
        filename = input1.nextLine();

        while(true)
        {        // Displaying Menu
            System.out.println(" \n\n 1. Write in a file \n 2. Read a file \n 3. Counting of words , characters and lines \n 4. Copy one file to another file \n 5. Rename the file \n 6. Exit \n");
            System.out.println("Enter your choice");
            choice = input1.nextInt();

            if(choice==1)
            {
                try
                {
                    FileWriter fout = new FileWriter(filename , true);// To write text in a file and also the file is appending

                    char ch;
                    Scanner input = new Scanner(System.in);
                    while((ch = (char)System.in.read())!= '\n')
                    {
                        fout.write(ch);// Writes the text into the file
                    }
                    fout.close();
                }
                catch(Exception e)
                {
                    System.out.println(e);
                }
            }

            else if(choice==2)
            {
                try
                {
                    FileReader fIn  = new FileReader(filename); //To read the text from the file
                    BufferedReader br = new BufferedReader(fIn);
                    String s;
                    while((s = br.readLine())!= null)   
                    {
                        System.out.print(s);
                    }
                }
                catch(Exception e)
                {
                    System.out.println(e);
                }

            }

            else if(choice==3) // To Count No. of lines,Words and Characters
            {
                try
                {                                         //Initialising the variables
                    int wordNum = 0;
                    int lineNum = 0;
                    int characterNum = 0;
                    Scanner input = new Scanner(new FileReader(filename)); // reads the file name from the user 

                    while(input.hasNextLine()) 
                    {
                        String line;
                        line  = input.nextLine();
                        lineNum++; // Counting number of lines
                        String str[] = line.split((" "));
                        for(int i = 0; i<str.length; i++)
                        {
                            if(str[i].length()>0) 
                            {
                                wordNum++; // Count Number of Words
                            }
                        }
                        characterNum += (line.length()); 
                    }

                    System.out.println(wordNum);
                    System.out.println(lineNum);
                    System.out.println(characterNum);

                    FileWriter output = new FileWriter(filename,true);

                    output.write("Words:" +wordNum);
                    output.write("Lines:" +lineNum);
                    output.write("Characters:" +characterNum);

                    input.close();
                    output.close();
                }

                catch(Exception e)
                {
                    System.out.println(e);
                }
            }

            else if(choice==4)  // To copy from one file to another
            {
                File infile = new File(filename);
                Scanner input = new Scanner(System.in);
                String file;
                System.out.println("Enter the file name in which you want to copy");
                file = input.nextLine();
                File outfile = new File(file);

                FileReader fr=null;
                FileWriter fw=null;

                try
                {
                    fr = new FileReader(infile);
                    fw = new FileWriter(outfile);
                    int ch;
                    while((ch= fr.read())!= -1)
                    {
                        fw.write(ch);
                        ch++;
                    }
                    fr.close();
                    fw.close();
                }
                catch(Exception e)
                {
                    System.out.println(e);
                }

            }

            else if(choice==5) //To Rename a file
            {
                File oldname = new File(filename);
                Scanner input = new Scanner(System.in);
                String file;
                System.out.println("Enter the changed file name you want to keep");
                file = input.nextLine();
                File newname = new File(file);
                boolean success = oldname.renameTo(newname);
                if(success)
                {
                    System.out.println("Renamed!!");
                }
                else
                {
                    System.out.println("Until you don't write in a file, file is not created so it cannot be renamed");
                }
            }

            else if(choice==6)
            {
                System.exit(0);
            }

			else
			{
				System.out.println("Your choice is wrong");
			}
        }


    }
}
