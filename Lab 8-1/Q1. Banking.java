/* You Have to Change System Date to Run The get INterest Function */

import java.util.*;
import java.io.*;
abstract class Account implements Serializable {                  //abstract parent class Account

private String accNo;
protected double balance;				//kept protected so that it can be used in it's child class

public Account(String no,double bal){	//Constructor of Account class
				accNo=no;
				balance=bal;
				}

public String get_accNo(){               // get Account No is common for savings and current therefore it has been kept in Account class
				return accNo;			 // return Accounts No
}
//to display balance and it is common for both child classes therefore it has been kept in parent class
public double get_balance()
{				return balance;			
}

public boolean deposit(double amount){	//deposit member function is common for savings and current therefore it has been kept in Account class
	
				amount=amount;
				if(amount>=0){			//If Amount is positive then Only Deposit is Successful 
						balance += amount;
						System.out.println("Deposit Successful"); 
						return true;
				}

				else{
						System.out.println("Deposit is Invalid!!");  // If Amount entered is negative than deposit is unsuccessful
						return false;
				}
			}

public abstract boolean withdraw(double amount1); //only prototype has been defined so that it can be implemented in dynamic polymorphism
	//kept abstract so that any user who would create other child classes of Account has to create this member function and declare it's method
	
public abstract boolean get_interest(int month);  // Get Interest method is Only For Savings Account but Since using object a1 we need to make 
													//in Account class as abstract
public abstract float get_InterestRate();		// returns Rate of Interest
public abstract double get_Odl();				// prototype to get OverDraft Limit
}

class Savings extends Account{                   //child class of Account

private float interestRate;
Calendar cale=Calendar.getInstance();
public Savings(String no,float Rate){			//Constructor of child class Savings
				super(no,1000);					//calling Constructor of parent class Account using 'super' keyword

				interestRate=Rate;
			}

public float get_InterestRate(){			// member Function to Show Interest Rate Already  set By the Bank
				return interestRate;
			}
public double get_Odl(){						// Body of get_Odl in Savings Class 
            System.out.println("This Facility is not available for Savings Account");
            return (000);						
}
public boolean withdraw(double amount1){       // withdraw Member Function for Savings Account
				if(amount1<=(balance-500)){		//Minimum Balance in the Account must be Rs 500 so used this 
							balance=balance - amount1;
							System.out.println("Withdraw Successful!!");
							return true;
						            }
				else
					{ System.out.println("Withdraw Unsuccessful!!");
						return false;
					}
				 }
public boolean get_interest(int month){       //to calculate interest
								double time=((((cale.get(Calendar.MONTH))+1)-month)/12f);
								double Rupees=(1+interestRate/400);	 
													if((((cale.get(Calendar.MONTH))+1)-month)>=3 && month<=12){ // Checks that difference in Month of Account created and the present Month is more than or =3  
																				balance=balance*Math.pow(Rupees,4*time);// Formula for Compound Interest is p*(1+r/n)^nt  where p=principle , r=rate of Interest,n=no. of Interests Compounded per Year,t=to calculate balance for t years  
								
								System.out.println("The Interest Has Been Added to account");   
								//System.out.println("Balance:"+balance); 
								//System.out.println("Rupees are:"+Rupees);
								return true;
								}
								else{
									balance=balance;
									return false;
									}
										}
}


class Current extends Account{  	//second child class of parent class Account

private double overDraft_lmt;
public Current(String no,double ovdl){ //Constructor of child class Current
	
				super(no,0);			//calling parent class Constructor of Account
				if((ovdl>=500)&&(ovdl<=10000000))				//overdraft limit has to be between "500"rs and "1Cr"rs.
	 					overDraft_lmt=ovdl;
				else{
                    System.out.println("Invalid OverDraft Limit");
                    overDraft_lmt = 500;    					// If overdraft Limit is out of range minimum amount is 500
				}}
		public double get_Odl(){
							return overDraft_lmt; 				// returns OverDraft Limit
					}
        public float get_InterestRate(){
                    System.out.println("This Facility is not available for Current Account");
                    return (999f);
					}
		public boolean withdraw(double amount1){				//withdraw member function for Current account
	
					if(amount1<=overDraft_lmt+balance){//customer can withdraw money till the negative balance upto  overdraft limit
			
							balance=balance-amount1;
							System.out.println("Withdraw Successful"); // If above Condition is satisfied than Withdraw is Successful
							return true;
									}
					else{
							System.out.println("Withdraw UnSuccessfull");
							return false;
						}
				}
		public boolean get_interest(int month){
			System.out.println("This Facility is not available for Current Account");
			return true;
	}
			}

class Banking{			//main class
public static void displayMenu1(){// Displaying the Options For the User
	System.out.println("Enter 1-->To Create New Account");
	System.out.println("Enter 2-->For Existing Account");
	System.out.println("Enter 3-->To Delete Account");
	System.out.println("Enter 4-->To Exit");
	System.out.println("Enter Your Choice: ");
	
	
	
	
}
public static void displayMenu(){   //Function to Display Menu 
			System.out.println("Enter 1-->  To Check Balance");
			System.out.println("Enter 2-->  To Deposit");
			System.out.println("Enter 3-->  To Withdraw");
			System.out.println("Enter 4-->  To Calculate the Interest And Balance After Certain period");
			System.out.println("Enter 5-->  To Get the Current Interest Rate");
			System.out.println("Enter 6-->  To Get the Current OverDraft Limit");
			System.out.println("Enter 7-->  Exit");
			System.out.println("Enter Your Choice...");

}

public static void main(String args[]) throws Exception{//main function
			Scanner input=new Scanner(System.in);
			int ch,ch1=0,choice;
			Account a1=null;//since Account is an abstract class therefore only it's reference can be created and we can't instantiate it
			String accNo=null;			
			boolean bool=false;
			while(true){
				displayMenu1();
				ch=input.nextInt();
				//Initialising all the input and Outputstreams to null
				FileInputStream fIn=null;
				FileOutputStream fOut=null;
				ObjectInputStream oin=null;
				ObjectOutputStream out=null;
				File f=null;
			
			if(ch==1){
						System.out.println("Enter 1-->  To Create Savings Account");
						System.out.println("Enter 2-->  To Create Current Account");
						ch1=input.nextInt();
						System.out.println("Enter Account Number: ");
						accNo=input.next();	
						f=new File(accNo+".dat"); // Creates a file with the Name of Account Number and with an extension of .dat
				if(ch1==1){		
					a1=new Savings(accNo,10f); // Default Account no is passed and rate of Interest is set to 10%
							}
				else if(ch1==2){
					a1=new Current(accNo,1000000); //Default Account no is passed and OverDraft Limit is set to 1o Lakh
				}
					FileOutputStream fout=null;
					ObjectOutputStream oout=null;
			try{
				fout=new FileOutputStream(accNo+".dat"); // Writes to a file 
				oout=new ObjectOutputStream(fout);// Writes an Object of a class to a file
				oout.writeObject(a1);
				}
			catch(Exception e)
			{
				System.out.println("The Error that occurred is:"+e);
			}
			finally
			{
				oout.close();
			}	
			while(true)
			{
				displayMenu();
				choice=input.nextInt();
				if(choice==1){ // to Check Balance
				try{// Try and Catch Blocks to Handle Exceptions
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();//type casting is done here
			System.out.println("The Current Balance :"+a1.get_balance());
			}
			catch(Exception e){
			System.out.println("The Error that occurred is "+e);
			}
			finally
			{		// Closes the input  Streams
				oin.close();
				fIn.close();	
			}
			}
			else if(choice==2){ // To Deposit 
			try
			{
				System.out.println("Enter Amount You want to Deposit");
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();
				a1.deposit(input.nextDouble());
				fOut=new FileOutputStream(f);
				oout=new ObjectOutputStream(fOut);
				oout.writeObject(a1);
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());
			}
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: ");
			}	
			finally
			{ // Closes the input and Output Streams
				oin.close();
				fOut.close();
				fIn.close();
				oout.close();
			}
				}
			else if(choice==3){ // To WithDraw
			try
			{
				System.out.println("Enter Amount You  want to Withdraw");
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();
				a1.withdraw(input.nextDouble());
				fOut=new FileOutputStream(f);
				oout=new ObjectOutputStream(fOut);
				oout.writeObject(a1);
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());
			
			}
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: "+e);
			}
			finally
			{
				fIn.close();
				fOut.close();
				oin.close();
				oout.close();
			}
								}
			else if(choice==4){
				if(ch1==1){					//To Get the Interest between the period of Month in which account is created and the present month and interest is added Quaterly
					try{
					System.out.println("Enter The Month in which you created the Account in the present Year");
					fIn=new FileInputStream(f);
					oin=new ObjectInputStream(fIn);
					a1=(Account)oin.readObject();
					a1.get_interest(input.nextInt());
					fOut=new FileOutputStream(f);
					oout=new ObjectOutputStream(fOut);
					oout.writeObject(a1);
					System.out.println("The Current Balance :"+a1.get_balance());
				}
				
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: "+e);
			}
			finally
			{
				fIn.close();
				fOut.close();
				oin.close();
				oout.close();
			}
				}
				else{
					System.out.println("This Facility Is Available Only For Savings Account");
				}

			}
			else if(choice==5){ // To get Interest Rate of Bank
				if(ch1==1){
					
						try
						{
							fIn = new FileInputStream (f);
							oin = new ObjectInputStream(fIn);
							a1 = (Account) oin.readObject();
							System.out.println(a1.get_InterestRate());// to know the interest rate
						}
						catch(Exception e)
						{
							System.out.println("The exception is" +e);
						}
						finally
						{
							oin.close();
							fIn.close();
						}
					System.out.println("The Current Interest Rate in % is:" +a1.get_InterestRate());
				}
				else{
					System.out.println(a1.get_InterestRate());
					}
            }
            else if(choice==6){
				if(ch1==1){					// To Know OverDraft limit provided to you by the Bank
				try
						{
							fIn = new FileInputStream (f);
							oin = new ObjectInputStream(fIn);
							a1 = (Account) oin.readObject();// TypeCasting 
							System.out.println(a1.get_Odl());// to know the overdraft limit

						}
						catch(Exception e)
						{
							System.out.println("The exception is" +e);
							displayMenu1();
						}
						finally
						{
							oin.close();
							fIn.close();
						}
					System.out.println("The Current OverDraft Limit is:" +a1.get_Odl());
				}
			else{
				System.out.println("The OverDraft Limit is Rs:"+a1.get_Odl());
				}

			}
			else { // To Exit
				System.exit(0);
			         }
				}
					}
	else if(ch==2)
	{
	System.out.println("Enter Your Account Number: ");
	accNo=input.next();
			if(new File(accNo.concat(".dat")).isFile()) // Checks if the file is present or not
				{
					f = new File(accNo.concat(".dat"));	
				while(true)
			{
				displayMenu();
				choice=input.nextInt();
				if(choice==1){ // to Check Balance
				try{
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();
			System.out.println("The Current Balance :"+a1.get_balance());
			}
			catch(Exception e){
			System.out.println("The Error that occurred is "+e);
			}
			finally
			{
				oin.close();
				fIn.close();	
			}
			}
			else if(choice==2){ // To Deposit 
			try
			{
				System.out.println("Enter Amount You want to Deposit");
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();
				a1.deposit(input.nextDouble());
				fOut=new FileOutputStream(f);
				out=new ObjectOutputStream(fOut);
				out.writeObject(a1);
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());
			}
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: ");
			}	
			finally
			{
				oin.close();
				fOut.close();
				fIn.close();
				out.close();
			}
				}
			else if(choice==3){ // To WithDraw
			try
			{
				System.out.println("Enter Amount You Withdraw");
				fIn=new FileInputStream(f);
				oin=new ObjectInputStream(fIn);
				a1=(Account)oin.readObject();
				a1.withdraw(input.nextDouble());
				fOut=new FileOutputStream(f);
				out=new ObjectOutputStream(fOut);
				out.writeObject(a1);
				System.out.println("Your Account No. :"+a1.get_accNo());
				System.out.println("The Current Balance :"+a1.get_balance());
			
			}
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: "+e);
			}
			finally
			{
				fIn.close();
				fOut.close();
				oin.close();
				out.close();
			}
								}
			else if(choice==4){
				if(ch1==1){   				//To Get the Interest between the period of Month in which account is created and the present month and interest is added Quaterly
					try{
					System.out.println("Enter The Month in which you created the Account in the present Year");
					fIn=new FileInputStream(f);
					oin=new ObjectInputStream(fIn);
					a1=(Account)oin.readObject();
					a1.get_interest(input.nextInt());
					fOut=new FileOutputStream(f);
					out=new ObjectOutputStream(fOut);
					out.writeObject(a1);
					System.out.println("The Current Balance :"+a1.get_balance());
				
				}
				
			catch(Exception e)
			{
				System.out.println("The Error that occurred is: "+e);
			}
			finally
			{
				fIn.close();
				fOut.close();
				oin.close();
				out.close();
			}
				}
				else{
					System.out.println("This Facility Is Available Only For Savings Account");
				}

			}
			else if(choice==5){ // To get Interest Rate of Bank
				if(ch1==1){
					
						try
						{
							fIn = new FileInputStream (f);
							oin = new ObjectInputStream(fIn);
							a1 = (Account) oin.readObject();
							System.out.println(a1.get_InterestRate());// to know the interest rate
						}
						catch(Exception e)
						{
							System.out.println("The exception is" +e);
						}
						finally
						{
							oin.close();
							fIn.close();
						}
					System.out.println("The Current Interest Rate in % is:" +a1.get_InterestRate());
				}
				else{
					System.out.println(a1.get_InterestRate());
					}
				}
            else if(choice==6){
				if(ch1==1){					// To Know OverDraft limit provided to you by the Bank
				try
						{
							fIn = new FileInputStream (f);
							oin = new ObjectInputStream(fIn);
							a1 = (Account) oin.readObject();
							System.out.println(a1.get_Odl());// to know the overdraft limit

						}
						catch(Exception e)
						{
							System.out.println("The exception is" +e);
							displayMenu1();
						}
						finally
						{
							oin.close();
							fIn.close();
						}
					System.out.println("The Current OverDraft Limit is:" +a1.get_Odl());
				}
				else{
				System.out.println("The OverDraft Limit is Rs:"+a1.get_Odl());
				}

			}
			else { // To Exit
				System.exit(0);
			         }
				}
				}
}
	else if(ch==3){
			System.out.println("Enter Account You Want to Delete");
			accNo=input.next();
			if(new File(accNo.concat(".ser")).isFile())
                {
					f = new File(accNo.concat(".ser"));
                    try
                    {
                        bool = f.delete();
                        System.out.println("File Has Been deleted!: "+bool);
                    }
                    catch(Exception e)
                    {
                        System.out.println("The exception is:" +e);
                    }

                }
                else
                {
                    System.out.println("Account does not exist!!!");
                }
		
				}
	else if(ch==4){
		System.exit(0);	
	}
else{
System.out.println("Please Enter a Valid Choice");
}
}

}
}