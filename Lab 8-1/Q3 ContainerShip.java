import java.util.*;
import java.io.*;

 class Engine{ 
 // Declaration of Variables
	static float power;
	static String engine_type;
	static float torque;
	static boolean trans_type;
	
	// Initializing variable in the default Constructor
	Engine(){
		power=0;
		engine_type="";
		torque=0;
		trans_type=false;	
			}
	//Assigning variables of parameterised Constructor		
	public Engine(float h_power,String e_type,float tor,boolean tra_type){
					power=h_power;
					engine_type=e_type;
					torque=tor;
					trans_type=tra_type;
	
    }// To get the horse Power
	public static float geth_p()
	{
		return power;
	}
	static String getengine_type()
	{
		return engine_type;
	}
	static float gettorque()
	{
		return torque;
	}	
	 static public boolean gettrans_type()
	{
		return trans_type;
	}
    // To Display all the Features of ENGINE
	public void Display_Engine(){
		System.out.println("The Horse Power of Engine:"+Engine.geth_p());
		System.out.println("The Engine Type is:"+Engine.getengine_type());
		System.out.println("The Torque of Engine:"+Engine.gettorque());
		System.out.println("The Transmission Type of Engine:"+Engine.gettrans_type());
								}																		  
			}
			
abstract class Vehicle{
	Scanner s=new Scanner(System.in);
	Engine eng=new Engine();  // Here Containership is used object of one class is created in other class which is not a mainclass
		//Declaring Variables of Vehicle class
	public String v_name;
	public String v_comp_name;
	public String v_colour;

	public void set_vehicle(){
	 	System.out.println("Enter The Name Of Vehicle: ");
		v_name=s.next();
		System.out.println("Enter The Company Name Of Vehicle: ");
		v_comp_name=s.next();
		System.out.println("Enter The Colour Of Vehicle: ");
		v_colour=s.next();	
							}	
	public void get_vehicle(){
		System.out.println("The Name of Vehicle is: "+v_name);
		System.out.println("The Company of Vehicle is: "+v_comp_name);
		System.out.println("The Colour  of Vehicle is: "+v_colour);
		eng.Display_Engine();
								}
					}
						
	class TwoWheeler extends Vehicle{// Class TwoWheeler inherits the property of Vehicle class
		String t_name,t_c_name;
		float avg;
		// Setters and Getters of this class
	public void set_twowheeler(){
		System.out.println("Enter Two Wheeler Company Name: ");
		t_c_name=s.next();
		System.out.println("Enter Two Wheeler Name: ");
		t_name=s.next();
		System.out.println("Enter Two Wheeler Average: ");
		avg=s.nextFloat();				
								}
	public void get_twowheeler(){
		System.out.println("Two Wheeler Company is: "+t_c_name);
		System.out.println("Two Wheeler Name is:"+t_name);
		System.out.println("Two Wheeler Average is:"+avg);
		
							  }
	void FileHandling(){
		try{
		FileOutputStream fout=new FileOutputStream("Vehicle Data.dat"); // File Named Vehicle Data gets created and object of file output stream is created
		ObjectOutputStream oout=new ObjectOutputStream(fout);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oout.writeObject(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oout.writeObject(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oout.writeObject(c);
		String tt="\n Transmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oout.writeObject(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oout.writeObject(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oout.writeObject(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oout.writeObject(g);
		String tc="\n Two Wheeler Vehicle Company Name: "+t_c_name;
		byte h[]=tc.getBytes();
		oout.writeObject(h);
		String tn="\n Two Wheeler Name: " +t_name;
		byte i[]=tn.getBytes();
		oout.writeObject(i);
		String ta="\n Two Wheeler Average: "+avg;
		byte j[]=ta.getBytes();
		oout.writeObject(j);
		oout.close();	
	}
		catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
		}
	}
void ReadFile(){
	try{
		FileInputStream fIn=new FileInputStream("Vehicle Data.dat");
		ObjectInputStream oin=new ObjectInputStream(fIn);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oin.read(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oin.read(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oin.read(c);
		String tt="\n Trasmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oin.read(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oin.read(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oin.read(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oin.read(g);
		String tc="\n Two Wheeler Vehicle Company Name: "+t_c_name;
		byte h[]=tc.getBytes();
		oin.read(h);
		String tn="\n Two Wheeler Name: " +t_name;
		byte i[]=tn.getBytes();
		oin.read(i);
		String ta="\n Two Wheeler Average: "+avg;
		byte j[]=ta.getBytes();
		oin.read(j);
		oin.close();			
	}
	catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
						}
					}	
				}
				
	class FourWheeler extends Vehicle{
		String f_name,f_c_name;
		float f_avg;
		
	public void set_fourwheeler(){
		System.out.println("Enter Four Wheeler Company Name: ");
		f_c_name=s.next();
		System.out.println("Enter Four Wheeler Name: ");
		f_name=s.next();
		System.out.println("Enter Four Wheeler Average: ");
		f_avg=s.nextFloat();				
								}
	public void get_fourwheeler(){
		System.out.println("Four Wheeler Company is: "+f_c_name);
		System.out.println("Four Wheeler Name is: "+f_name);
		System.out.println("Four Wheeler Average is: "+f_avg);
		
								}
								
									}
	class CommFourWheeler extends FourWheeler{
		int no_flaps;
		int airbags;
		int extra_wheel=2;
		
		public void set_commfourwheeler(){
			System.out.println("Enter Number of Mud flaps:");
			no_flaps=s.nextInt();
			System.out.println("Enter Number of Air Bags Needed: ");
			airbags=s.nextInt();
										}
		public void get_commfourwheeler(){
			System.out.println("The Number of Mud Flaps for Vehicle is/are:"+no_flaps);
			System.out.println("The Number of Air Bags for Vehicle is/are:"+airbags);
			System.out.println("The Number of Spare Wheel for Vehicle is/are:"+extra_wheel);	
										}
		void FileHandling(){
		try{
		FileOutputStream fout=new FileOutputStream("Vehicle Data.dat");
		ObjectOutputStream oout=new ObjectOutputStream(fout);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oout.writeObject(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oout.writeObject(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oout.writeObject(c);
		String tt="\n Trasmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oout.writeObject(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oout.writeObject(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oout.writeObject(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oout.writeObject(g);
		String fc="\n Four Wheeler Vehicle Company Name: "+f_c_name;
		byte h[]=fc.getBytes();
		oout.writeObject(h);
		String fn="\n Four Wheeler Name: " +f_name;
		byte i[]=fn.getBytes();
		oout.writeObject(i);
		String fa="\n Four Wheeler Average: "+f_avg;
		byte j[]=fa.getBytes();
		oout.writeObject(j);
		String nf="\n Number Of Mud Flaps(Commercial car): "+no_flaps;
		byte k[]=nf.getBytes();
		oout.writeObject(k);
		String na="\n Number of Air Bags(Commercial Car): "+airbags;
		byte l[]=na.getBytes();
		oout.writeObject(l);
		String ew="\n Number Of Spare Wheel/Wheels(Commercial Car): "+extra_wheel;
		byte m[]=ew.getBytes();
		oout.writeObject(m);
		
		oout.close();	
	}
		catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
		}
	}
void ReadFile(){
	try{
		FileInputStream fIn=new FileInputStream("Vehicle Data.dat");
		ObjectInputStream oin=new ObjectInputStream(fIn);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oin.read(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oin.read(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oin.read(c);
		String tt="\n Trasmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oin.read(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oin.read(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oin.read(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oin.read(g);
		String fc="\n Four Wheeler Vehicle Company Name: "+f_c_name;
		byte h[]=fc.getBytes();
		oin.read(h);
		String fn="\n Four Wheeler Name: " +f_name;
		byte i[]=fn.getBytes();
		oin.read(i);
		String fa="\n Four Wheeler Average: "+f_avg;
		byte j[]=fa.getBytes();
		oin.read(j);
		String nf="\n Number Of Mud Flaps(Commercial car): "+no_flaps;
		byte k[]=nf.getBytes();
		oin.read(k);
		String na="\n Number of Air Bags(Commercial Car): "+airbags;
		byte l[]=na.getBytes();
		oin.read(l);
		String ew="\n Number Of Spare Wheel/Wheels(Commercial Car): "+extra_wheel;
		byte m[]=ew.getBytes();
		oin.read(m);
		
		oin.close();			
	}
	catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
						}
					}	
				}
	//asgerjrykl
	class PrivateFourWheeler extends FourWheeler{
		int no_flaps;
		int airbags;
		int extra_wheel=1;
		
		public void set_prifourwheeler(){
			System.out.println("Enter Number of Mud flaps:");
			no_flaps=s.nextInt();
			System.out.println("Enter Number of Air Bags Needed: ");
			airbags=s.nextInt();
										}
		public void get_prifourwheeler(){
			System.out.println("The Number of Mud Flaps for Vehicle is/are:"+no_flaps);
			System.out.println("The Number of Air Bags for Vehicle is/are:"+airbags);
			System.out.println("The Number of Spare Wheel for Vehicle is/are:"+extra_wheel);	
										}
		void FileHandling(){
		try{
		FileOutputStream fout=new FileOutputStream("Vehicle Data.dat");
		ObjectOutputStream oout=new ObjectOutputStream(fout);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oout.writeObject(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oout.writeObject(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oout.writeObject(c);
		String tt="\n Trasmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oout.writeObject(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oout.writeObject(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oout.writeObject(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oout.writeObject(g);
		String fc="\n Private Four Wheeler Vehicle Company Name: "+f_c_name;
		byte h[]=fc.getBytes();
		oout.writeObject(h);
		String fn="\n Private Four Wheeler Name: " +f_name;
		byte i[]=fn.getBytes();
		oout.writeObject(i);
		String fa="\n Private Four Wheeler Average: "+f_avg;
		byte j[]=fa.getBytes();
		oout.writeObject(j);
		String nf="\n Number Of Mud Flaps(Private car): "+no_flaps;
		byte k[]=nf.getBytes();
		oout.writeObject(k);
		String na="\n Number of Air Bags(Private Car): "+airbags;
		byte l[]=na.getBytes();
		oout.writeObject(l);
		String ew="\n Number Of Spare Wheel/Wheels(Private Car): "+extra_wheel;
		byte m[]=ew.getBytes();
		oout.writeObject(m);
		
		oout.close();	
	}
		catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
		}
	}
void ReadFile(){
	try{
		FileInputStream fIn=new FileInputStream("Vehicle Data.dat");
		ObjectInputStream oin=new ObjectInputStream(fIn);
		String hp="\n Horse Power: "+Engine.geth_p();
		byte a[]=hp.getBytes();
		oin.read(a);
		String et="\n Engine Type: "+Engine.getengine_type();
		byte b[]=et.getBytes();
		oin.read(b);
		String tq="\n Engine Torque: "+Engine.gettorque();
		byte c[]=tq.getBytes();
		oin.read(c);
		String tt="\n Trasmission Type:(Boolean) "+Engine.gettrans_type();
		byte d[]=tt.getBytes();
		oin.read(d);
		String vc="\n Vehicle Company Name: "+v_comp_name;
		byte e[]=vc.getBytes();
		oin.read(e);
		String vn="\n Vehicle Name: "+v_name;
		byte f[]=vn.getBytes();
		oin.read(f);
		String vcl="\n Vehicle Colour: "+v_colour;
		byte g[]=vcl.getBytes();
		oin.read(g);
		String fc="\n Four Wheeler Vehicle Company Name: "+f_c_name;
		byte h[]=fc.getBytes();
		oin.read(h);
		String fn="\n Four Wheeler Name: " +f_name;
		byte i[]=fn.getBytes();
		oin.read(i);
		String fa="\n Four Wheeler Average: "+f_avg;
		byte j[]=fa.getBytes();
		oin.read(j);
		String nf="\n Number Of Mud Flaps(Private car): "+no_flaps;
		byte k[]=nf.getBytes();
		oin.read(k);
		String na="\n Number of Air Bags(Private Car): "+airbags;
		byte l[]=na.getBytes();
		oin.read(l);
		String ew="\n Number Of Spare Wheel/Wheels(Private Car): "+extra_wheel;
		byte m[]=ew.getBytes();
		oin.read(m);
		
		oin.close();			
	}
	catch(Exception e){
		System.out.println("An Error Occured!!!"+e);
						}
					}	
				}
	
class ContainerShip{
	public static void main(String args[]){
		Scanner s=new Scanner(System.in);
System.out.println(".........................Welcome To BMW Showroom...............");		
		while(true){
			TwoWheeler tw=new TwoWheeler();
			FourWheeler fw=new FourWheeler();
			Vehicle v;
			System.out.println("Enter 1-->To Buy Two Wheeler\nEnter 2-->To Buy Four Wheeler\nEnter 3-->To Exit\nEnter Your Choice");
			int choice=s.nextInt();
	if(choice==1){
		System.out.println("Engine Horse Power:");
		float x=s.nextFloat();
		System.out.println("Engine Type: ");
		String y=s.next();
		System.out.println("Engine Torque: ");
		float z=s.nextFloat();
		System.out.println("Engine Transmission Type:(Boolean) ");
		boolean w=s.nextBoolean();
		Engine eng=new Engine(x,y,z,w);
		v=tw;
		v.set_vehicle();
		tw.set_twowheeler();
		System.out.println("\n");
		v.get_vehicle();
		tw.get_twowheeler();
		tw.FileHandling();
		tw.ReadFile();
		System.out.println("File Has Been Created Successfully!!!");
		break;
			}
		else if(choice==2){
		do{
			System.out.println("Enter 1-->To Buy Commercial Vehicle\nEnter 2-->To Buy Private\nEnter Your Choice: ");
		int ch=s.nextInt();
		if(ch==1){	
		System.out.println("Engine Horse Power:");
		float x1=s.nextFloat();
		System.out.println("Engine Type: ");
		String y1=s.next();
		System.out.println("Engine Torque: ");
		float z1=s.nextFloat();
		System.out.println("Engine Transmission Type:(Boolean) ");
		boolean w1=s.nextBoolean();
		Engine eng=new Engine(x1,y1,z1,w1);
		v=fw;
		v.set_vehicle();
		fw.set_fourwheeler();
		CommFourWheeler cfw= new CommFourWheeler();
		cfw.set_commfourwheeler();
		v.get_vehicle();
		fw.get_fourwheeler();
		cfw.get_commfourwheeler();
		cfw.FileHandling();
		cfw.ReadFile();
		System.out.println("File Has Been Created Successfully!!!");
		break;	
			}
		else if(ch==2){
		System.out.println("Engine Horse Power:");
		float x2=s.nextFloat();
		System.out.println("Engine Type: ");
		String y2=s.next();
		System.out.println("Engine Torque: ");
		float z2=s.nextFloat();
		System.out.println("Engine Transmission Type: ");
		boolean w2=s.nextBoolean();
		Engine eng1=new Engine(x2,y2,z2,w2);
		v=fw;
		v.set_vehicle();
		fw.set_fourwheeler();
		PrivateFourWheeler pfw= new PrivateFourWheeler();
		pfw.set_prifourwheeler();
		v.get_vehicle();
		fw.get_fourwheeler();
		pfw.get_prifourwheeler();
		pfw.FileHandling();
		pfw.ReadFile();
		System.out.println("File Has Been Created Successfully!!!");
		break;		
		}
		
		}while(choice!=0);	
			
			
		}	
	else if(choice==3)
	{
	System.exit(0);	
		
	}
	else{
		System.out.println("Please Enter A Valid Choice");
		
	}
			
		}
			
	}	
	
}											
											