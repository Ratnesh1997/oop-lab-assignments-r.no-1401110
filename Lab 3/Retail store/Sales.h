#ifndef SALES_H
#define SALES_H

namespace customer
{


class Sales //class Declaration for Sales account program
{    //variables declaration
    private:
        struct Sales *next;
        struct Sales *top;
        struct Sales *tail;
    public:
        char item_name[25];
        float prod_price,total_cost;
        Sales();
        int Quantity;
        void getData();  // member function which are made outside class
		void putData();
};
}
#endif // SALES_H
