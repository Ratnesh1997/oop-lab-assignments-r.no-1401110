#ifndef STACK_H
#define STACK_H

namespace Mystack
{

struct node
{
    int info;
    struct node *next;
};
class stack{
public:
    struct node *top,*top1,*temp;
    int top_element();
    void push(int data);
    void pop();
    void empty();
    void display();
    void stack_count();
     stack();
     ~stack();
    int count = 0;
};
}
#endif // STACK_H
