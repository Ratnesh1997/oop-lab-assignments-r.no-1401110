#include<iostream>
#include<cstdlib>
#include "stack.h"
#include "stack.cpp"
#define STACK_H
using namespace std;
using namespace Mystack;
int  main()
{
    int no,ch,e;
            stack obj;
    cout<<"**********\t \t STACK PROGRAM- ASSIGNMENT 2\t \t********** "<<endl;
    cout<<"\n Enter  1 -> Push";
    cout<<"\n Enter  2 -> Pop";
    cout<<"\n Enter  3 -> Top";
    cout<<"\n Enter  4 -> Empty";
    cout<<"\n Enter  5 -> Stack Count";
    cout<<"\n Enter  6 -> Display";
    cout<<"\n Enter  7 -> Exit";

    stack();

    while (1)
    {
        cout<<"\n Enter choice : ";
        cin>>ch;

        switch (ch)
        {
        case 1:
            cout<<"\n Enter data : ";
            cin>>no;
            obj.push(no);
            break;
        case 2:
            obj.pop();
            break;
        case 3:
            if (obj.top == NULL)
                cout<<"It is an Empty stack";
            else
            {
                e=obj.top_element();
                cout<<"\n Top element :"<<e;
            }
            break;
        case 4:
            obj.empty();
            break;
        case 7:
            exit(0);
        case 6:
            obj.display();
            break;
        case 5:
            obj.stack_count();
            break;
        default :
            cout<<" Please Enter correct choice  ";
            break;
        }
    }
}

