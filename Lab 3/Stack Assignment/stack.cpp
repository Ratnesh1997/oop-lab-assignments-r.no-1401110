#include "stack.h"
#include<iostream>
#include<malloc.h>
using namespace std;
using namespace Mystack;// namespace is used

/* Create empty stack */
//using Constructor
stack::stack()
{
    top = NULL;
}
//Using destructor
stack::~stack(){
delete top;
}
/* Count stack elements */
void stack::stack_count()
{
    cout<<"\n No. of elements in stack : "<<count;
}

/* Push data into stack */
void stack::push(int data)
{
    if (top == NULL)
    {
        top =(struct node *)malloc(1*sizeof(struct node));//Allocating memory to new node
        top->next = NULL;
        top->info = data;
    }
    else
    {   temp= new node;//node of dynamic length
        temp =(struct node *)malloc(1*sizeof(struct node));

        temp->next = top;
        temp->info = data;
        top = temp;
    }
    count++;
}

/* Display stack elements */
void stack::display()
{
    top1 = top;

    if (top1 == NULL)
    {
        cout<<"Stack is empty";
        return;
    }

    while (top1 != NULL)
    {
        cout<< "\t "<<top1->info<<endl;
        top1 = top1->next;
    }
 }

/* Pop Operation on stack */
void stack::pop()
{
    top1 = top;

    if (top1 == NULL)
    {
        cout<<"\n Error : ITS AN EMPTY STACK ";
        return;
    }
    else
        top1 = top1->next;
    cout<<"\n Popped value : "<<top->info;
    free(top);
    top = top1;
    count--;
}

/* Return top element */
int stack::top_element()
{
    return(top->info);
}

/* Check if stack is empty or not */
void stack::empty()
{
    if (top == NULL)
        cout<<"\n Stack is empty";
    else
        cout<<"\n Stack is not empty and has elements="<<count;
}
