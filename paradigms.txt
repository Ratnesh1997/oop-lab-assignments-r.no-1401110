The different types of paradigms in programming language:

A programming paradigm can basically be defined as the way of building the structure and elements of computer programs.
The paradigms of a computer programming language are the capabilities and styles of it.Some programming languages follow
single paradigm others  support multiple paradigms.

The lowest level programming paradigms are machine code which directly represents the contents of phone memory as a 
sequence of numbers.These are sometimes called first and second generation languages.List of common paradigms are:
Imperative,Declarative,Structured,Procedural,Functional,Object-Oriented,logical,event-driven,flow-driven,reflective etc.
The major programming paradigms are:

1.The Imperative: In this programming paradigm computations are performed
through a guided sequence of steps,in which the variables are referred to or changed.The order of the steps is crucial,
because a given step will have different consequences depending on the current values of variables when the step is 
executed.The imperative paradigm is more often used than other paradigms because it is efficient , popular and close to
the machine.It's disadvantages are that its somewhat complex,there are some side effects while debugging etc.
Languages that emphasize this paradigm are:C,C++,Java,COBOL,etc.

2.The logical:It takes a declarative approach to problem solving.Various logiical assertions are made establishing all
known facts.It's advantages are : 1 The programming steps becomes minimum. 2 Proving the validity of a program becomes simple.
Any deducible solution to a query is returned.Languages that emphasize this paradigm are:Prolog,GHC,Parlog,Vulcan,Polka,Mercury.

3 Functional: The functional programming paradigm views all subprograms as functions in the mathematical sense informally,
they take in arguements and return a single solution.The computational model is therefore one of function applcation and 
reduction.It's advantages are that it removes the possibility of errors,and due to its indepency on assignment operations
the programs are allowed to be ecvaluated in many orders.It has comparitively less efficiency.Languages that emphasize this
paradigm are:Scheme,Haskell,Miranda,ML,FP,FL,J.Javascript is an example of a hybrid functional/imperative paradigms.

4.Object-Oriented:In this paradigm the real-world objects are vieed as seperate identities which are modified only by
built-in procedures,called methods.Objects are  organized into classes, from which they inherit methods and variables.
This paradigm provides key benefits of reusable code and code extensibility.One of the  distinguishing feature is tha ability 
to use inheritance.Languages that emphasize this paradigm are: C++,CLOS,Eiffel,Modula-3,Ada 95,Java,C#,Ruby.

REFERENCES:1.Wikipedia
           2.cs.lmu.edu/~ray/notes/paradigms
	   3.www.perlmuonks.org
 	   4.stackoverflow.com
	   5.www.eecs.ucf and some reference book etc.
	  